/**
 * Created by Вячеслав on 11.03.2015.
 */

angular.module('graph')
  .service('PersonalService', function () {

      this.staffList = {
        1 : {
         name : 'Owner'
        },
        2 : {
          name: 'Top Manager1'
        },
        3: {
          name: 'Top Manager2'
        },
        4: {
          name : 'Manager'
        },
        5: {
          name : 'Team Lead'
        },
        6: {
          name: 'Dev1'
        },
        7: {
          name: 'Dev2'
        },
        8: {
          name: 'Qa1'
        },
        9:{
          name: 'Уборщица'
        }
      }

      this.hierarchy = {
        1: {
          level: 0,
          slaves: {
              2: {
                level: 1,
                slaves: {
                  4: {
                    level: 2,
                    slaves: {
                      6: {
                        level: 3,
                        slaves: {
                          9: {
                            level: 4,
                            slaves: {}
                          }
                        }
                      },
                      8: {
                        level: 3,
                        slaves: {}
                      }
                    }
                  },
                  5: {
                    level: 2,
                    slaves: {
                      6: {
                        level: 3,
                        slaves: {}
                      },
                      7: {
                        level: 3,
                        slaves: {}
                      }
                    }
                  }
                }
              },
              3: {
                level: 1,
                slaves: {}
              }
          }
          }
        },
        this.getData = function(){

      return this.hierarchy;
    }

      this.getStaffNameById = function(id){
        console.log(id, 'id');
        console.log(this.staffList, 'this.staffList');
        console.log(this.staffList[id.toString()], 'this.staffList[id.toString()]');

        return this.staffList[id.toString()].name;
      }

    this.getStaff = function(){

    }.bind(this);
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
