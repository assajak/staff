/**
 * Created by Вячеслав on 11.03.2015.
 */
'use strict';

angular.module('graph')
  .controller('PersonalCtrl', function ($scope, PersonalService) {
    this.staffList = PersonalService.getData();
  });
