/**
 * Created by Вячеслав on 15.03.2015.
 */

'use strict';

angular.module('graph')
  .directive('staff', function (PersonalService) {
    return {
      restrict: 'AE',
      scope: {
        staffList: "="
      },
      link: function (scope, element, attrs) {

        scope.$watch('staffList', function(staffList){

          console.log(staffList, 'staffList');

          var body = d3.select('personal')
          var svg = body.append('svg').attr('height', 1000).attr('width', 1000);
          var levels = [];
          var blockHeight = 75;
          var blockWidth = 150;
          var textMargin = 20;

          function getStaff(row, prevCoords) {

            var prevCoords = prevCoords || null;

            if (Object.keys(row).length !== 0) {
              var keys = Object.keys(row);
              for (var id in keys) {
                levels.push(row[keys[id]].level)

                var x = getCountOfElementsInArray(levels, row[keys[id]].level)*200;
                var y = (row[keys[id]].level + 1) * 150;

                svg.append('rect').attr('width', 150)
                  .attr('height', blockHeight)
                  .attr('width', blockWidth)
                  .attr('x', x)
                  .attr('y', y)
                  .style('fill', 'white')
                  .attr('stroke', 'black')

                svg.append('text').text(PersonalService.getStaffNameById(keys[id]))
                  .attr('x',x+textMargin)
                  .attr('y', y+blockHeight/2)
                  .attr('fill', 'black')

                if (prevCoords){
                  svg.append('line')
                    .attr('x1', prevCoords.x+blockWidth/2)
                    .attr('x2', x+blockWidth/2)
                    .attr('y1', prevCoords.y+blockHeight)
                    .attr('y2', y)
                    .attr('stroke', 'red')
                    .attr('stroke-width', '2');
                }

                getStaff(row[keys[id]].slaves, {x: x, y: y});
              }
            }
          }

          getStaff(staffList);

        });

      }
    };
  });

function getCountOfElementsInArray(array, element){
  var count = 0;

  for (var id in array){
    if (array[id] === element){
      count++;
    }
  }

  return count;
}
